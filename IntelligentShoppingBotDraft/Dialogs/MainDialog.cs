// Copyright (c) Microsoft Corporation. All rights reserved.
// Licensed under the MIT License.
//
// Generated with Bot Builder V4 SDK Template for Visual Studio CoreBot v4.9.1

using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Bot.Builder;
using Microsoft.Bot.Builder.Dialogs;
using Microsoft.Bot.Schema;
using Microsoft.Extensions.Logging;
using Microsoft.Recognizers.Text.DataTypes.TimexExpression;

using IntelligentShoppingBotDraft.CognitiveModels;

namespace IntelligentShoppingBotDraft.Dialogs
{
    public class MainDialog : ComponentDialog
    {
        private readonly FlightBookingRecognizer _luisRecognizer;
        protected readonly ILogger Logger;
        private static string attachmentPromptId1 = $"{nameof(UploadImageSuggestionDialog)}_attachmentPrompt1";
        // Dependency injection uses this constructor to instantiate MainDialog
        public MainDialog(FlightBookingRecognizer luisRecognizer, UploadImageSuggestionDialog uploadImageSuggestionDialog, BookingDialog bookingDialog, ILogger<MainDialog> logger)
            : base(nameof(MainDialog))
        {
            _luisRecognizer = luisRecognizer;
            Logger = logger;

            AddDialog(new TextPrompt(nameof(TextPrompt)));
            AddDialog(new AttachmentPrompt(nameof(AttachmentPrompt)));
            AddDialog(new AttachmentPrompt(attachmentPromptId1));
            AddDialog(bookingDialog);
            AddDialog(uploadImageSuggestionDialog);
            AddDialog(new WaterfallDialog(nameof(WaterfallDialog), new WaterfallStep[]
            {
                IntroStepAsync,
                ActStepAsync,
                FinalStepAsync,
            }));

            // The initial child Dialog to run.
            InitialDialogId = nameof(WaterfallDialog);
        }

        private async Task<DialogTurnResult> IntroStepAsync(WaterfallStepContext stepContext, CancellationToken cancellationToken)
        {
            if (!_luisRecognizer.IsConfigured)
            {
                await stepContext.Context.SendActivityAsync(
                    MessageFactory.Text("NOTE: LUIS is not configured. To enable all capabilities, add 'LuisAppId', 'LuisAPIKey' and 'LuisAPIHostName' to the appsettings.json file.", inputHint: InputHints.IgnoringInput), cancellationToken);

                return await stepContext.NextAsync(null, cancellationToken);
            }

            // Use the text provided in FinalStepAsync or the default if it is the first time.


            //var card = new HeroCard
            //{
            //    Text = "Hi , I am Shopalchohlic assitant.I'll help you with your shopping today",
            //    Buttons = new List<CardAction>
            //    {
            //        // Note that some channels require different values to be used in order to get buttons to display text.
            //        // In this code the emulator is accounted for with the 'title' parameter, but in other channels you may
            //        // need to provide a value for other parameters like 'text' or 'displayText'.
            //        new CardAction(ActionTypes.ImBack, title: "1. I know what I want (Upload Picture)", value: "1"),
            //        new CardAction(ActionTypes.ImBack, title: "2. I know what I want, but I dont have picture", value: "2"),
            //        new CardAction(ActionTypes.ImBack, title: "3. Unsure what to buy, can I suggest something?", value: "3"),
            //    },
            //};

            //var reply = MessageFactory.Attachment(card.ToAttachment());
           // await stepContext.p

            var messageText = "Select any one and let's accomplish mission shopping!";
           // var messageText = stepContext.Options?.ToString() ?? "What can I help you with today?\nSay something like \"Book a flight from Paris to Berlin on March 22, 2020\"";
            var promptMessage = MessageFactory.Text(messageText, messageText, InputHints.ExpectingInput);
            return await stepContext.PromptAsync(nameof(TextPrompt), new PromptOptions { Prompt = promptMessage }, cancellationToken);
        }

        private async Task<DialogTurnResult> ActStepAsync(WaterfallStepContext stepContext, CancellationToken cancellationToken)
        {
            if (!_luisRecognizer.IsConfigured)
            {
                // LUIS is not configured, we just run the BookingDialog path with an empty BookingDetailsInstance.
                return await stepContext.BeginDialogAsync(nameof(BookingDialog), new BookingDetails(), cancellationToken);
            }

            // Call LUIS and gather any potential booking details. (Note the TurnContext has the response to the prompt.)
            var luisResult = await _luisRecognizer.RecognizeAsync<ShoppingAssistant>(stepContext.Context, cancellationToken);
            switch (luisResult.TopIntent().intent)
            {
                case ShoppingAssistant.Intent.Greeting:
                    // We haven't implemented the GetWeatherDialog so we just display a TODO message.
                    var greetingMessageText = "Hi, how are you?";
                    var greetingMessage = MessageFactory.Text(greetingMessageText, greetingMessageText, InputHints.IgnoringInput);
                    await stepContext.Context.SendActivityAsync(greetingMessage, cancellationToken);
                    break;

                case ShoppingAssistant.Intent.BookFlight:
                    await ShowWarningForUnsupportedCitiesShoppingAssistant(stepContext.Context, luisResult, cancellationToken);

                    // Initialize BookingDetails with any entities we may have found in the response.
                    var bookingDetails = new BookingDetails()
                    {
                        // Get destination and origin from the composite entities arrays.
                        Destination = luisResult.ToEntities.Airport,
                        Origin = luisResult.FromEntities.Airport,
                        TravelDate = luisResult.TravelDate,
                    };

                    // Run the BookingDialog giving it whatever details we have from the LUIS call, it will fill out the remainder.
                    return await stepContext.BeginDialogAsync(nameof(BookingDialog), bookingDetails, cancellationToken);

                case ShoppingAssistant.Intent.GetWeather:
                    // We haven't implemented the GetWeatherDialog so we just display a TODO message.
                    var getWeatherMessageText = "TODO: get weather flow here";
                    var getWeatherMessage = MessageFactory.Text(getWeatherMessageText, getWeatherMessageText, InputHints.IgnoringInput);
                    await stepContext.Context.SendActivityAsync(getWeatherMessage, cancellationToken);
                    break;
              
                case ShoppingAssistant.Intent.Farewell:
                    // We haven't implemented the GetWeatherDialog so we just display a TODO message.
                    var fareWellMessageText = "Bye, Speak to you soon!";
                    var fareWellMessage = MessageFactory.Text(fareWellMessageText, fareWellMessageText, InputHints.IgnoringInput);
                    await stepContext.Context.SendActivityAsync(fareWellMessage, cancellationToken);
                    break;
                case ShoppingAssistant.Intent.Selection:

                    var selectionOption = luisResult.Text;
                  
                    // Run the BookingDialog giving it whatever details we have from the LUIS call, it will fill out the remainder.
                    //var bookingDetails1 = new BookingDetails()
                    return await stepContext.BeginDialogAsync(nameof(UploadImageSuggestionDialog), new BookingDetails(), cancellationToken);

                   // break;
                //case "1":
                //    var fareWellMessageText = "Bye, Speak to you soon!";
                //    break;

                //case "2":
                //    var fareWellMessageText = "Bye, Speak to you soon!";
                //    break;

                default:
                    // Catch all for unhandled intents
                    var didntUnderstandMessageText = $"Sorry, I didn't get that. Please try asking in a different way (intent was {luisResult.TopIntent().intent})";
                    var didntUnderstandMessage = MessageFactory.Text(didntUnderstandMessageText, didntUnderstandMessageText, InputHints.IgnoringInput);
                    await stepContext.Context.SendActivityAsync(didntUnderstandMessage, cancellationToken);
                    break;
            }

            return await stepContext.NextAsync(null, cancellationToken);
        }

        // Shows a warning if the requested From or To cities are recognized as entities but they are not in the Airport entity list.
        // In some cases LUIS will recognize the From and To composite entities as a valid cities but the From and To Airport values
        // will be empty if those entity values can't be mapped to a canonical item in the Airport.
        private static async Task ShowWarningForUnsupportedCities(ITurnContext context, FlightBooking luisResult, CancellationToken cancellationToken)
        {
            var unsupportedCities = new List<string>();

            var fromEntities = luisResult.FromEntities;
            if (!string.IsNullOrEmpty(fromEntities.From) && string.IsNullOrEmpty(fromEntities.Airport))
            {
                unsupportedCities.Add(fromEntities.From);
            }

            var toEntities = luisResult.ToEntities;
            if (!string.IsNullOrEmpty(toEntities.To) && string.IsNullOrEmpty(toEntities.Airport))
            {
                unsupportedCities.Add(toEntities.To);
            }

            if (unsupportedCities.Any())
            {
                var messageText = $"Sorry but the following airports are not supported: {string.Join(',', unsupportedCities)}";
                var message = MessageFactory.Text(messageText, messageText, InputHints.IgnoringInput);
                await context.SendActivityAsync(message, cancellationToken);
            }
        }
        private static async Task ShowWarningForUnsupportedCitiesShoppingAssistant(ITurnContext context, ShoppingAssistant luisResult, CancellationToken cancellationToken)
        {
            var unsupportedCities = new List<string>();

            var fromEntities = luisResult.FromEntities;
            if (!string.IsNullOrEmpty(fromEntities.From) && string.IsNullOrEmpty(fromEntities.Airport))
            {
                unsupportedCities.Add(fromEntities.From);
            }

            var toEntities = luisResult.ToEntities;
            if (!string.IsNullOrEmpty(toEntities.To) && string.IsNullOrEmpty(toEntities.Airport))
            {
                unsupportedCities.Add(toEntities.To);
            }

            if (unsupportedCities.Any())
            {
                var messageText = $"Sorry but the following airports are not supported: {string.Join(',', unsupportedCities)}";
                var message = MessageFactory.Text(messageText, messageText, InputHints.IgnoringInput);
                await context.SendActivityAsync(message, cancellationToken);
            }
        }
        private async Task<DialogTurnResult> FinalStepAsync(WaterfallStepContext stepContext, CancellationToken cancellationToken)
        {
            //Find out if any attachments are given
            bool imageAttached = false;
            if(stepContext.Context.Activity.Attachments!=null)
            {
                imageAttached = true;

            }

            if(imageAttached)
            {
                // Restart the main dialog with a different message the second time around
                var promptMessage = "You have attached Image";
                return await stepContext.ReplaceDialogAsync(InitialDialogId, promptMessage, cancellationToken);
            }
            else
            {
                // If the child dialog ("BookingDialog") was cancelled, the user failed to confirm or if the intent wasn't BookFlight
                // the Result here will be null.
                if (stepContext.Result is BookingDetails result)
                {
                    // Now we have all the booking details call the booking service.

                    // If the call to the booking service was successful tell the user.

                    var timeProperty = new TimexProperty(result.TravelDate);
                    var travelDateMsg = timeProperty.ToNaturalLanguage(DateTime.Now);
                    var messageText = $"I have you booked to {result.Destination} from {result.Origin} on {travelDateMsg}";
                    var message = MessageFactory.Text(messageText, messageText, InputHints.IgnoringInput);
                    await stepContext.Context.SendActivityAsync(message, cancellationToken);
                }
                // Restart the main dialog with a different message the second time around
                var promptMessage = "What else can I do for you?";
                return await stepContext.ReplaceDialogAsync(InitialDialogId, promptMessage, cancellationToken);
            }

          

          
        }
    }
}
